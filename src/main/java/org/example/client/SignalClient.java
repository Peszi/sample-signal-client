package org.example.client;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SignalClient {

    static ExecutorService pool = Executors.newFixedThreadPool(2);

    private Process process;
    private PrintWriter writer;

    public SignalClient(String msisdn) {
        ProcessBuilder builder = new ProcessBuilder("tools/signal-cli-0.12.4/bin/signal-cli.bat", "-a", msisdn, "jsonRpc");
        try {
            process = builder.start();

            pool.submit(new ProcessReadTask(System.err, process.getErrorStream()));
            pool.submit(new ProcessReadTask(System.out, process.getInputStream()));

            writer = new PrintWriter(new OutputStreamWriter(
                    process.getOutputStream(), StandardCharsets.UTF_8), true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String sendMessage(String recipient, String line) {
        var uuid = UUID.randomUUID().toString();
        writer.println("{\"jsonrpc\":\"2.0\",\"method\":\"send\",\"params\":{\"recipient\":[\"+48736013513\"],\"message\":\"" + line + "\"},\"id\":\"" + uuid + "\"}");
//        writer.flush();
        return uuid;
    }

    public void shutdown() {
        pool.shutdown();
        writer.close();
        process.destroy();
    }

    static class ProcessReadTask implements Runnable {

        private final PrintStream printStream;
        private final InputStream inputStream;

        public ProcessReadTask(PrintStream printStream, InputStream inputStream) {
            this.printStream = printStream;
            this.inputStream = inputStream;
        }

        @Override
        public void run() {
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, StandardCharsets.UTF_8))) {
                String line;
                while ((line = reader.readLine()) != null) {
                    printStream.println(line);
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

}
