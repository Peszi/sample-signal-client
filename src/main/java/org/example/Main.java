package org.example;

import org.example.client.SignalClient;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        var client = new SignalClient("+48883017183");

        Scanner scanner = new Scanner(System.in);
        String line;
        while (!(line = scanner.nextLine()).isEmpty()) {

            System.out.println(">" + line);
            client.sendMessage("+48736013513", line);
        }

    }
}